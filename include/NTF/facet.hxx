#pragma once


#include <NTF/top.hxx>


_BEGIN_NTF


/*!
 *
 */
class _NTF_IMPORT _Facet_base {
  public:
    /*!
     *
     */
    virtual _NTF_THIS_CALL ~_Facet_base() _NTF_NO_EXCEPT {
    }

    
    /*!
     *
     */
    virtual void _NTF_THIS_CALL remain() = 0;

    
    /*!
     *
     */
    virtual void _NTF_THIS_CALL release() = 0;
};


_END_NTF
