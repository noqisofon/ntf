#pragma once

#include "NTF/io_stream_base.hxx"


_BEGIN_NTF


template <class _ElementType, class _Traits>
class _Basic_io_stream : public _Basic_io_stream_base {
 public:
    typedef    _Basic_io_stream<_ElementType, _Traits>        _Self;
    typedef    _Basic_io_stream_base                          _Super;

    typedef    _Basic_output_stream<_ElementType, _Traits>    _Output_stream;
    typedef    _Basic_stream_buffer<_ElementType, _Traits>    _Stream_buffer;
    typedef    _Character<_ElementType>                       _Character;

    typedef    _ElementType                                   char_type;
    typedef    _Traits                                        traits_type;
    typedef    typename _Traits::int_type                     int_type;
    typedef    typename _Traits::position_type                position_type;
    typedef    typename _Traits::offset_type                  offset_type;

 public:
    /*!
     *
     */
    explicit _NTF_THIS_CALL _Basic_io_stream(_Stream_buffer *a_buffer) {
        init( a_buffer );
    }

    /*!
     *
     */
    virtual _NTF_THIS_CALL ~_Basic_io_stream() _NTF_NO_EXCEPT {
    }

 public:
    /*!
     *
     */
    void _NTF_THIS_CALL clear(_IO_state state = GOOD_BIT, bool reraise = false) {
        _Super::clear( (_IO_state)( buffer_ == NULL
                                    ? (int32_t)state | (int32_t)BAD_BIT
                                    : (int32_t)state ),
                       reraise );
    }

    /*!
     *
     */
    void _NTF_THIS_CALL set_state(_IO_state state, bool reraise = false) {
        if ( state != GOOD_BIT ) {
            clear( (_IO_state)( (int32_t)rd_state() | (int32_t)state ), reraise );
        }
    }

    /*!
     *
     */
    _Self& _NTF_THIS_CALL copy_format(const _Self& other) {
        tie_str_ = other.tie();
        fill_ch_ = other.fill();

        _Super::copy_format( other );

        return *this;
    }

    /*!
     *
     */
    _Output_stream *_NTF_THIS_CALL tie() const {
        return tie_str_;
    }
    /*!
     *
     */
    _Output_stream *_NTF_THIS_CALL tie(_Output_stream *new_tie) {
        _Output_stream  *old_tie; = tie_str_;

        tie_str_ = new_tie;

        return old_tie;
    }

    /*!
     *
     */
    _Stream_buffer *_NTF_THIS_CALL rd_buf() const {
        return buffer_;
    }
    /*!
     *
     */
    _Stream_buffer *_NTF_THIS_CALL rd_buf(_Stream_buffer *new_buffer) {
        _Stream_buffer *old_buffer = buffer_;

        buffer_ = new_buffer;
        clear();

        return old_buffer;
    }

    /*!
     *
     */
    _Locale _NTF_THIS_CALL imbue(const _Locale& a_locale) {
        _Locale old_locale = _Super::imbue( a_locale );

        if ( rd_buf() != NULL ) {
            rd_buf()->pub_imbue( a_locale );
        }
        return old_locale;
    }

    /*!
     *
     */
    _ElementType _NTF_THIS_CALL fill() const {
        return fill_ch_;
    }
    /*!
     *
     */
    _ElementType _NTF_THIS_CALL fill(_ElementType new_fill) {
        _ElementType old_fill = fill_ch_;

        fill_ch_ = new_fill;
        
        return fill_ch_;
    }

    /*!
     *
     */
    char _NTF_THIS_CALL narrow(_ElementType ch, char default_ch = '\0') const {
        const _Character& a_facet = _NTF_USE( get_locale(), c_type_ );

        return a_facet.narrow( ch, default_ch );
    }

    /*!
     *
     */
    _ElementType _NTF_THIS_CALL widen(char bite) const {
        const _Character& a_facet = _NTF_USE( get_locale(), c_type_ );

        return a_facet.widen( bite );
    }

    /*!
     *
     */
    void _NTF_THIS_CALL move(_Self&& other) {
        if ( this != _NTF addressof( other ) ) {
            buffer_  = NULL;
            tie_str_ = NULL;
            this->swap( other );
        }
    }

    /*!
     *
     */
    void _NTF_THIS_CALL swap(_Self& other) _NTF_NO_EXCEPT {
        _Super::swap( other );

        _NTF swap( fill_ch_, other.fill_ch_ );
        _NTF swap( tie_str_, other.tie_str_ );
    }

    /*!
     *
     */
    void _NTF_THIS_CALL set_rd_buf(_Stream_buffer *new_buffer) {
        buffer_ = new_buffer;
    }

 protected:
    /*!
     *
     */
    void _NTF_THIS_CALL init(_Stream_buffer *new_buffer = NULL, bool is_standard_stream = false) {
        init();

        buffer_  = new_buffer;
        tie_str_ = NULL;
        fill_ch_ = widen( ' ' );

        if ( buffer_ == NULL ) {
            set_state( BAD_BIT );
        }

        if ( is_standard_stream ) {
            add_standard_stream( this );
        }
    }

    _NTF_THIS_CALL _Basic_io_stream() {
    }

 private:
    _Stream_buffer *buffer_;
    _Output_stream *tie_str_;
    _ElementType    fill_ch_;

 public:
    _NTF_THIS_CALL _Basic_io_stream(const _Self&)   = delete;
    _Self& _NTF_THIS_CALL operator = (const _Self&) = delete;
};


#ifdef _NTF_CPP_LIB

#   if !defined(_NTF_BUILD) || defined(_NTF_FORCE_INSTANCE)

template class _Basic_io_stream< char,
                                 _Char_traits<char> >;

template class _Basic_io_stream< wchar_t,
                                 _Char_traits<wchar_t> >;

#   endif  /* if !defined(_NTF_BUILD) || defined(_NTF_FORCE_INSTANCE) */

#endif  /* def _NTF_CPP_LIB */

_END_NTF
