#pragma once

#include <stddef.h>
#include <stdint.h>

#define    _NTF_IMPORT
#define    _NTF_THIS_CALL

#ifdef _MSC_VER
#   define    _NTF_DECL          __cdecl
#else
#   define    _NTF_DECL
#endif  /* def _MSC_VER */

#define    _NTF_NO_EXCEPT

#if defined(__cplusplus)
#   define    _BEGIN_NTF    namespace ntf {
#   define    _END_NTF      }
#   define    _NTF          ::ntf::
#else
#   define    _BEGIN_NTF
#   define    _END_NTF
#   define    _NTF
#endif  /* defined(__cplusplus) */
