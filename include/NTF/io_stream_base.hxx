#pragma once

#include "NTF/locale.hxx"
#include "NTF/system_error.hxx"


_BEGIN_NTF


/*!
 *
 */
class _NTF_IMPORT _Basic_io_stream_base {
 public:
    typedef    int32_t    _Format_flags;
    typedef    int32_t    _IO_state;
    typedef    int32_t    _Open_mode;
    typedef    int32_t    _Seek_direction;

    enum _Event {
        ERASE_EVENT,
        IMBUE_EVENT,
        COPY_FORMAT_EVENT
    };

    typedef void (_NTF_THIS_CALL *_Event_callback)(_Event, _Basic_io_stream_base&, int32_t);

    /*!
     *
     */
    class _Failure : public _System_error {
     public:
        /*!
         *
         */
        explicit _Failure(const _String&      message,
                          const _Error_code&  error_code = NTF_make_error_code( _IO_err::STREAM ) )
            : _System_error( error_code, message ) {
        }
        /*!
         *
         */
        explicit _Failure(const char         *message,
                          const _Error_code&  error_code = NTF_make_error_code( _IO_err::STREAM ) )
            : _System_error( error_code, message ) {
        }
    };

    /*!
     *
     */
    class _NTF_IMPORT _Initializer {
      public:
        /*!
         *
         */
        _NTF_THIS_CALL _Initializer() {
            _initializer_ctor( this );
        }
        
        /*!
         *
         */
        _NTF_THIS_CALL ~_Initializer() _NTF_NO_EXCEPT {
            _initializer_dtor( this );
        }

      private:
        static void     _NTF_DECL _initializer_ctor(_Initializer *);
        static void     _NTF_DECL _initializer_dtor(_Initializer *);

        static int32_t _initializer_count;

        static int32_t& _NTF_DECL _initializer_count_fn();
    };
};


_END_NTF
