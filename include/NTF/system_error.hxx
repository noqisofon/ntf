#pragma once

#include <NTF/cerrno.hxx>


_BEGIN_NTF


enum error_code {
    INTERRPTED     = EINTR,
    IO_ERROR       = EIO,
};


enum class _IO_err {
    STREAM         = 1
};

class _Error_code {
 public:
    typedef    _Error_code    _Self;

 public:
    /*!
     *
     */
    _Error_code() _NTF_NO_EXCEPT : code_( 0 ), category_( &NTF_system_category() ) {
    }
    /*!
     *
     */
    _Error_code(int32_t code, const _Error_category& category) _NTF_NO_EXCEPT : code_( code ), category_( &category ) {
    }
    /*!
     *
     */
    template <class _Enum,
              class = _Enable_if<is_error_code_enum_v<_Enum>>>
    _Error_code(_Enum error_code) _NTF_NO_EXCEPT : code_( 0 ), category_( 0 ) {
        *this = NTF_make_error_code( error_code );
    }

 public:
    /*!
     *
     */
    void assign(int32_t code, const _Error_category& category) _NTF_NO_EXCEPT {
        code_     = code;
        category_ = category;
    }
    
    /*!
     *
     */
    template <class _Enum,
              class = _Enable_if<is_error_code_enum_v<_Enum>>>
    _Error_code& operator = (_Enum error_code) _NTF_NO_EXCEPT {
        *this = NTF_make_error_code( error_code );

        return *this;
    }

    /*!
     *
     */
    void clear() _NTF_NO_EXCEPT {
        code_     = 0;
        category_ = &NTF_system_category();
    }

    /*!
     *
     */
    int32_t value() const _NTF_NO_EXCEPT {
        return code_;
    }

    /*!
     *
     */
    const _Error_category& category() const _NTF_NO_EXCEPT {
        return *category_;
    }

    /*!
     *
     */
    _Error_condition defaultErrorCondition() const _NTF_NO_EXCEPT;

    /*!
     *
     */
    _String message() const {
        return category().message( value() );
    }

    /*!
     *
     */
    explicit operator bool() const {
        return value() != 0;
    }

 private:
    int32_t                   code_;
    const _Error_category*    category_;
};


_END_NTF
