#include "NTF/io_stream_base.hxx"
using namespace ntf;


void     _NTF_DECL _Basic_io_stream_base::_Initializer::_initializer_ctor(_Initializer *) {
}

void     _NTF_DECL _Basic_io_stream_base::_Initializer::_initializer_dtor(_Initializer *) {
}

int32_t _Basic_io_stream_base::_Initializer::_initializer_count = 0;


int32_t& _NTF_DECL _Basic_io_stream_base::_Initializer::_initializer_count_fn() {
    return _initializer_count;
}
