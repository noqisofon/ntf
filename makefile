.PHONY: syntax-check


syntax-check:
	g++ -W -Wall -Wextra -fsyntax-only -I./include ./src/io_stream_base.cpp
